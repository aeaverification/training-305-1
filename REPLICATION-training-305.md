# [traning-305] [STATISTICAL NON-SIGNIFICANCE IN EMPIRICAL ECONOMICS] Validation and Replication results

SUMMARY
-------

- Only Figure 4 is replicated, with minor bugs.

General
-------

> [SUGGESTED] A recommended README template for replication packages in economics can be found on the [Social Science Data Editor Github site](https://social-science-data-editors.github.io/guidance/template-README.html).

Data description
----------------

### Data Sources

econ_experiments_data
- The dataset is provided by the author
- 18×4 data table. Records in rows 1-18 are the 18 experiments of section III in the article. Fields in columns 1-4 are z-statistic in the original study, number of participants in the original study, z-statistic in replication study, and number of participants in the replication study, respectively.
- The data are not cited in both the paper and the README

### Analysis Data Files

- [x] No analysis data file mentioned
- [ ] Analysis data files mentioned, not provided (explain reasons below)
- [ ] Analysis data files mentioned, provided. File names listed below.

Data deposit
------------

### Requirements 

> INSTRUCTIONS: Check that these requirements are met. 

- [x] README is in TXT, MD, PDF format
- [x] openICPSR deposit has no ZIP files
- [ ] Title conforms to guidance (starts with "Data and Code for:" or "Code for:", is properly capitalized)
- [ ] Authors (with affiliations) are listed in the same order as on the paper

> [REQUIRED] Please ensure that a ASCII (txt), Markdown (md), or PDF version of the README are available in the data and code deposit.
> [REQUIRED] Please review the title of the openICPSR deposit as per our guidelines (below).
> INSTRUCTIONS: Leave the following line in the report if any of the above are checked:
> Detailed guidance is at [https://aeadataeditor.github.io/aea-de-guidance/](https://aeadataeditor.github.io/aea-de-guidance/). 

### Deposit Metadata

- [ ] JEL Classification (required)
- [ ] Manuscript Number (required)
- [ ] Subject Terms (highly recommended)
- [ ] Geographic coverage (highly recommended)
- [ ] Time period(s) (highly recommended)
- [ ] Collection date(s) (suggested)
- [ ] Universe (suggested)
- [ ] Data Type(s) (suggested)
- [ ] Data Source (suggested)
- [ ] Units of Observation (suggested)

Data checks
-----------

- Data can be read using standard tools, Data is in EXCEL format.
- Dataset does not have variable labels, but comes with a separate documentation TXT file.

Code description
----------------

There are two provided Matlab.m files.

- The main analysis programs are written in Matlab and the could be executed through the given Matlab.m program "econ_experiments.m".
- Figure 4: can be replicated by running the given program "econ_experiments.m" and the auxiliary Matlab.m file "line_fewer_markers.m".
- The program codes and the README both identify which tables are produced by what program.
- No code is provided for Figure 1, 2, 3, A.1, A.2
[SUGGESTED] Please specify hardware requirements, and duration (execution time) for the last run, to allow replicators to assess the computational requirements.

Stated Requirements
---------------------

- [ ] No requirements specified
- [x] Software Requirements specified as follows:
  - Excel
  - Matlab
- [ ] Computational Requirements specified as follows:
- [ ] Time Requirements specified as follows:
- [x] Requirements are complete.

Computing Environment of the Replicator
---------------------

- CISER Shared Windows Server 2019, 256GB, Intel Xeon E5-4669 v3 @ 2.10Ghz (2 processors, 36 cores)
- Matlab R2019a

Replication steps
-----------------

1. Downloaded code and data from openICPSR.
2. Ran code as per README, but the minor bugs appeared.
3. Made changes to the way the third step is run to get it to work.

Findings
--------

### Data Preparation Code

- Program `line_fewer_markers.m` failed to produce any output.

### Tables

- N/A

### Figures

- Figure 1: Data and program not given
- Figure 2: Data and program not given
- Figure 3: Data and program not given
- Figure 4: Fully replicated
- Figure A.1: Data and program not given
- Figure A.2: Data and program not given

### In-Text Numbers

[ ] There are no in-text numbers, or all in-text numbers stem from tables and figures.
[x] There are in-text numbers, but they are not identified in the code

Classification
--------------

- [x] full reproduction
- [ ] full reproduction with minor issues
- [ ] partial reproduction (see above)
- [ ] not able to reproduce most or all of the results (reasons see above)

### Reason for incomplete reproducibility

- [ ] `Discrepancy in output` (either figures or numbers in tables or text differ)
- [ ] `Bugs in code`  that  were fixable by the replicator (but should be fixed in the final deposit)
- [ ] `Code missing`, in particular if it  prevented the replicator from completing the reproducibility check
  - [ ] `Data preparation code missing` should be checked if the code missing seems to be data preparation code
- [ ] `Code not functional` is more severe than a simple bug: it  prevented the replicator from completing the reproducibility check
- [ ] `Software not available to replicator`  may happen for a variety of reasons, but in particular (a) when the software is commercial, and the replicator does not have access to a licensed copy, or (b) the software is open-source, but a specific version required to conduct the reproducibility check is not available.
- [ ] `Insufficient time available to replicator` is applicable when (a) running the code would take weeks or more (b) running the code might take less time if sufficient compute resources were to be brought to bear, but no such resources can be accessed in a timely fashion (c) the replication package is very complex, and following all (manual and scripted) steps would take too long.
- [ ] `Data missing` is marked when data *should* be available, but was erroneously not provided, or is not accessible via the procedures described in the replication package
- [ ] `Data not available` is marked when data requires additional access steps, for instance purchase or application procedure. 